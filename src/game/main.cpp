
#include "StringUtils.h"
#include "SocketAddress.h"
#include "SocketAddressFactory.h"
#include "UDPSocket.h"
#include "TCPSocket.h"
#include "SocketUtil.h"

#if _WIN32
#include <Windows.h>
#endif

#include <iostream>

#if _WIN32
int main(int argc, const char** argv)
{
	
	UDPSocketPtr server;
	
	SocketUtil::StaticInit();

	const SocketAddressPtr toAddress = SocketAddressFactory::CreateIPv4FromString("127.0.0.1:7");
	const SocketAddressPtr fromAddress = shared_ptr<SocketAddress>(new SocketAddress()) ;
	
	server = SocketUtil::CreateUDPSocket(SocketAddressFamily::INET);

	string stringrtr("Hello World");

	int writecount = server->SendTo(static_cast<const void*>(stringrtr.c_str()), stringrtr.length() + 1, *toAddress);

	const int BUFF_MAX = 32;
	char buff[BUFF_MAX];

	int readCount = server->ReceiveFrom(static_cast<void*>(buff), BUFF_MAX, *fromAddress);


	if (fromAddress != nullptr) 
	{
		std::cout << "Address (not String format):" << fromAddress->getIP4() << std::endl;
		std::cout << "Port:" << fromAddress->getPort() << std::endl;
		std::cout << "Socket Family:" << fromAddress->getFamily() << std::endl;
		std::cout << "Buffer Content:" << buff << std::endl;



	}


	SocketUtil::CleanUp();
}
#else
const char** __argv;
int __argc;
int main(int argc, const char** argv)
{
	__argc = argc;
	__argv = argv;

}
#endif
