<!-- 
0.1 Original version 2019-2020. 
1.0 Updated version, running with this again for 2021-2022
1.1 Fixing issues with google test. 
-->

# Cross Platform Sockets Wrapper #

This is a small cross platform sockets wrapper (plus some testing), most of the core code is based on [Multiplayer Game Programming: Architecting Networked Games](https://www.oreilly.com/library/view/multiplayer-game-programming/9780134034355/) - by Josh Glazer.  This project is looking to extract out the core library allowing students in the third year Multi-Player Game Development module to use it in their own games having read the relevant book chapters.

**Updated 4-1-2022:** 
- Moved to using the FetchContent plugin for gtest. 
- Moved top level CMake to the root of the project (this seems to be the convention). 

## Targets ##

### Libraries ###

**networking** - TCP/UDP networking library and supporting data structures.  This is a wrapper around BSD Sockets and WindSock which should work across platforms.

**strings** - Error logging etc.

### Executable Targets ###

**game** - From the book, planning to use this in the future.

**TCPEchoClient** - An echo client.  The code base contains two alternatives, one which makes one connection and sends one string to be echo'd the other behaves more like a Telnet client connecting to an echo server.

**TCPEchoServer** - An echo Server.  Again there are two alternatives, the first echoes one string (and is more a tutorial/demo) the second behaves more like an echo server.

**NetworkGame_test** - An automatically named target making use of GoogleTest (the Google Unit Test Framework).  This is included to allow students to get some experience of unit testing and popular tools. It also provides an excellent source of documentation as the code in the tests can be used to understand the various classes used in the examples.

## Known Issues ##
1. Testing is incomplete
	- Linux Testing - **DONE**
	- Windows Testing - **DONE**
		- Works in the labs (with ports open and servies started)
		- Works on Windows laptop (as above). 
		<!-- Need to link install guide for this -->
2. Some tests won't work on the university network (or your home machines!)
	- ~~Connect -- tries to contact an external 'echo' server.~~
	- Works provided windows/linux local network services are running on the machine. 
		- [Linux](http://www.yolinux.com/TUTORIALS/LinuxTutorialNetworking.html#INET)
			- Some linux distributions no longer provide these servers, so we have to make our own. See [here](https://nmap.org/ncat/guide/ncat-simple-services.html) <!-- Glenn ... make a script for this -->
		- [Windows](https://teckangaroo.com/tcpip-services-how-to-enable-tcp-ip-services-on-windows-10/)
3. Some tests require manual input
	- Listen -- waits for a connection on port 54321
	- Echo tests for TCP/UDP
	- **CTest** integration will mark these as failed unless the info can be sent (does all the tests at once!). 

## Notes ##
<!--
** Questions for Students - remove before going live **
	* The client / server both struggle work out how much data to expect.
	* How could this be overcome?
-->